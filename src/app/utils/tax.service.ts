import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaxService {

  private flatRate: number = 0.7;
  private taxIncomeRate: number = 0.15;
  private maxCosts: number = 300_000;

  constructor() { }

  private getFlatRateTaxBase(yearlyIncome: number): number {
      return yearlyIncome - Math.min(yearlyIncome * this.flatRate, this.maxCosts);
  }

  public getYearlyIncomeTax(yearlyIncome: number): number {
      return this.getFlatRateTaxBase(yearlyIncome) * this.taxIncomeRate;
  }

  public getMontlyIncomeTax(monthlyIncome: number, taxRate: number): number {
    return this.getFlatRateTaxBase(monthlyIncome) * taxRate;
  }
}
