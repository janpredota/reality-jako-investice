import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UnitService {
    
    public static getUnit(amount: number, unitType: UnitType): string {
        switch (unitType) {
            case UnitType.czechCurrency:
                return 'Kč';
            case UnitType.czechCurrencyPerMonth:
                return 'Kč / měs';
            case UnitType.czechCurrencyPerYear:
                return 'Kč / rok';
            case UnitType.percentage:
                return '%';
            case UnitType.years:
                if (amount > 4) {
                    return 'let';
                } else if (amount > 1) {
                    return 'roky';
                } else {
                    return 'rok';
                }
        }
    }
    
    public static getUnitWithoutTime(amount: number, unitType: UnitType): string {
        if (unitType === UnitType.czechCurrencyPerMonth || unitType === UnitType.czechCurrencyPerYear) {
            return this.getUnit(amount, UnitType.czechCurrency);
        } else {
            return this.getUnit(amount, unitType);
        }
    }
}

export enum UnitType {
    czechCurrency,
    years,
    percentage,
    czechCurrencyPerMonth,
    czechCurrencyPerYear,
}
