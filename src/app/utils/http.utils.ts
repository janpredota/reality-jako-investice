import { Range } from "../formsControls/range/range";

export class HttpUtils {


  public static setParamOrDefaultValue(params: any, range: Range) {
    range.value = +params[range.parameterName] || range.value;
    if (range.optional) {
      range.checked = params[range.parameterName] !== undefined || range.checked;
    }
    if (params[`${range.parameterName}Visible`] == "true") {
      range.hidden = false;
    } else {
      range.hidden = true;
    }
  }
}