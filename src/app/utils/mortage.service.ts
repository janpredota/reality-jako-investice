import { Injectable } from '@angular/core';
import { MathUtils } from './math.utils';

@Injectable({
  providedIn: 'root'
})
export class MortgageService {

  constructor() { }

  public static getMonthlyPayment(loanAmount: number, interestRateYear: number, longTerm: number): number {

    if (interestRateYear === 0) {
      return Math.round(MathUtils.valueOrZero(loanAmount / (longTerm * 12)));
    }

    let interestRateMonth = interestRateYear / 12;                                          
    return Math.round(MathUtils.valueOrZero((loanAmount * interestRateMonth * (1 + interestRateMonth) ** (12 * longTerm)) / ((1 + interestRateMonth) ** (12 * longTerm) - 1)));
  }

  public static getMyMonthlyPayment(monthlyMortgagePayment: number, monthlyNettIncome: number): number {
    return monthlyMortgagePayment > monthlyNettIncome ? monthlyMortgagePayment - monthlyNettIncome : 0;
  }
}
