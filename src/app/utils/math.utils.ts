export class MathUtils {
    public static valueOrZero(value: number): number {
        return value > 0 ? value : 0;
    }
}