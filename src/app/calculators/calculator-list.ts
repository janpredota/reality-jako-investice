import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-list-of-calculators',
  template: `<nav>
                <a routerLink="/calculator" routerLinkActive="active">Všechny kalkulačky</a>
                <a routerLink="/calculator/rental-investment" routerLinkActive="active">Nemovistost na pronájem</a>
              </nav>`
})
export class CalculatorList {

  listOfCalculators = [
    {
      name: 'Investiční kalkulačka na pronájem',
      routerLink: 'rental-investment'
    }
  ]
}