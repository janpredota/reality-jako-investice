import { Component, OnInit, QueryList } from '@angular/core';
import { MatSelectChange, MatOption } from '@angular/material';

@Component({
  selector: 'app-calculator',
  template: `
    <section class="gray_bg">
      <div class="container">
          <!-- <router-outlet></router-outlet> -->
          <app-rental-investment></app-rental-investment>
      </div>
    </section>
    <app-articles [onlyFirstFew]="3"></app-articles>`
})
export class CalculatorComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {

  }

}