import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { faShare } from '@fortawesome/free-solid-svg-icons';
import { UnitType } from 'src/app/utils/unit.service';
import { ActivatedRoute } from '@angular/router';
import { Range } from 'src/app/formsControls/range/range';
import { RentalInvestmentRangeService } from 'src/app/formsControls/range/rental-investment-range.service';
import { TranslatePipe } from 'src/app/services/translate.pipe';
import { RangeGroup } from 'src/app/formsControls/range/rangeGroup';
import { ShareService } from 'src/app/services/share.service';
import { MatSnackBar } from '@angular/material';
import { RentalService } from 'src/app/services/rental.service';
import { RentalPropertyCalculations } from 'src/app/models/rental-property-calculations';
import { HttpUtils } from 'src/app/utils/http.utils';

@Component({
  selector: 'app-rental-investment',
  templateUrl: './rental-investment.component.html',
  styleUrls: ['./rental-investment.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RentalInvestmentComponent implements AfterViewInit {

  maxValueCount: number = 0;

  unitType = UnitType;
  faShare = faShare;

  propertyPrice: Range = new Range();
  ownCapital: Range = new Range();
  interestRate: Range = new Range();
  longTerm: Range = new Range();
  expectedRental: Range = new Range();
  commision: Range = new Range();
  rentalFees: Range = new Range();
  lawPartner: Range = new Range();
  taxOwnership: Range = new Range();
  insuranceFlat: Range = new Range();
  insuranceMortage: Range = new Range();
  taxPurchase: Range = new Range();
  taxRental: Range = new Range();
  
  colorSchemeFinancing = {
    domain: ['#bbe1fa', '#1b262c', '#ff880e']
  };
  
  colorSchemePayment = {
    domain: ['#1b262c', '#ff880e']
  };

  propertyRanges: RangeGroup = {
    name: 'InvestmentCosts',
    ranges: [this.propertyPrice, this.ownCapital, this.commision]
  }

  bankRanges: RangeGroup = {
    name: 'BankLoan',
    ranges: [this.interestRate, this.longTerm, this.insuranceMortage]
  }

  rentalRanges: RangeGroup = {
    name: 'RentalConditions',
    ranges: [this.expectedRental, this.rentalFees, this.lawPartner, this.insuranceFlat]
  }

  taxRanges: RangeGroup = {
    name: 'Taxes',
    ranges: [this.taxOwnership, this.taxPurchase, this.taxRental]
  }

  ranges: RangeGroup[] = [this.propertyRanges, this.bankRanges, this.rentalRanges, this.taxRanges];

  calculations: RentalPropertyCalculations = new RentalPropertyCalculations();

  showRoa: boolean = true;
  disableAnimation: boolean = true;

  constructor(
    private route: ActivatedRoute,
    rangeService: RentalInvestmentRangeService,
    private translate: TranslatePipe,
    private shareService: ShareService,
    private snackBar: MatSnackBar,
    private rentalService: RentalService,
  ) {

    this.propertyPrice = Range.copyRangeValue(rangeService.propertyPrice, this.propertyPrice);
    this.ownCapital = Range.copyRangeValue(rangeService.ownCapital, this.ownCapital);
    this.interestRate = Range.copyRangeValue(rangeService.interestRate, this.interestRate);
    this.longTerm = Range.copyRangeValue(rangeService.longTerm, this.longTerm);
    this.expectedRental = Range.copyRangeValue(rangeService.expectedRental, this.expectedRental);
    this.commision = Range.copyRangeValue(rangeService.commision, this.commision);
    this.rentalFees = Range.copyRangeValue(rangeService.rentalFees, this.rentalFees);
    this.lawPartner = Range.copyRangeValue(rangeService.lawPartner, this.lawPartner);
    this.taxOwnership = Range.copyRangeValue(rangeService.taxOwnership, this.taxOwnership);
    this.insuranceFlat = Range.copyRangeValue(rangeService.insuranceFlat, this.insuranceFlat);
    this.insuranceMortage = Range.copyRangeValue(rangeService.insuranceMortage, this.insuranceMortage);
    this.taxPurchase = Range.copyRangeValue(rangeService.taxPurchase, this.taxPurchase);
    this.taxRental = Range.copyRangeValue(rangeService.taxRental, this.taxRental);

    this.route.queryParams.subscribe(params => {
      if (Object.keys(params).length > 0) {
        this.taxPurchase.checked = false;
        this.commision.checked = false;
        this.rentalFees.checked = false;
        this.taxRental.checked = false;
        this.taxOwnership.checked = false;
        this.insuranceFlat.checked = false;
      }
      HttpUtils.setParamOrDefaultValue(params, this.propertyPrice);
      HttpUtils.setParamOrDefaultValue(params, this.ownCapital);
      HttpUtils.setParamOrDefaultValue(params, this.interestRate);
      HttpUtils.setParamOrDefaultValue(params, this.longTerm);
      HttpUtils.setParamOrDefaultValue(params, this.expectedRental);
      HttpUtils.setParamOrDefaultValue(params, this.insuranceMortage);
      HttpUtils.setParamOrDefaultValue(params, this.taxPurchase);
      HttpUtils.setParamOrDefaultValue(params, this.commision);
      HttpUtils.setParamOrDefaultValue(params, this.rentalFees);
      HttpUtils.setParamOrDefaultValue(params, this.lawPartner);
      HttpUtils.setParamOrDefaultValue(params, this.taxRental);
      HttpUtils.setParamOrDefaultValue(params, this.taxOwnership);
      HttpUtils.setParamOrDefaultValue(params, this.insuranceFlat);
    });

    this.recalculate();
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.disableAnimation = false);
  }

  public recalculate(): void {

    this.calculations.propertyPrice = this.propertyPrice.getValue();
    this.calculations.ownCapital = this.ownCapital.getValue();
    this.calculations.comission = this.commision.getValue();
    this.calculations.interestRate = this.interestRate.getValue();
    this.calculations.longTerm = this.longTerm.getValue();
    this.calculations.mortgageInsurance = this.insuranceMortage.getValue();
    this.calculations.expectedRental = this.expectedRental.getValue();
    this.calculations.rentalFees = this.rentalFees.getValue();
    this.calculations.lawPartner = this.lawPartner.getValue();
    this.calculations.propertyInsurance = this.insuranceFlat.getValue();
    this.calculations.ownershipTax = this.taxOwnership.getValue();
    this.calculations.purchaseTaxRate = this.taxPurchase.getValue();
    this.calculations.rentalTaxRate = this.taxRental.getValue();

    this.rentalService.calculate(this.calculations);
  }

  round(value: number): string {
    return `${Math.round(value)}`;
  }

  valueFormatting(value: number): string {
    return `${Math.round(value).toLocaleString()} Kč`;
  }

  valueFormattingPercentage(value: number): string {
    return `${Math.round(value * 100 * 10) / 10}%`;
  }

  pieTooltipText({ data }) {
    const label = data.name;
    const val = data.value;

    return `
      <span class="tooltip-label">${label}</span>
      <span class="tooltip-val">${val.toLocaleString()} Kč</span>
    `;
  }

  getInvestmentChartData(): any {
    return [
      {
        'name': this.translate.transform('InitialInvestment'),
        'value': this.calculations.initialInvestment,
        'share': this.calculations.initialInvestment / this.calculations.investmentCostsTotal,
        'color': '#bbe1fa'
      },
      {
        'name': this.translate.transform('AdditionalInvestment'),
        'value': this.calculations.additionalInvestment,
        'share': this.calculations.additionalInvestment / this.calculations.investmentCostsTotal,
        'color': '#1b262c'
      },
      {
        'name': this.translate.transform('PaydByRental'),
        'value': this.calculations.investmentCostsTenant,
        'share': this.calculations.investmentCostsTenant / this.calculations.investmentCostsTotal,
        'color': '#ff880e'
      }
    ]
  }

  getMortgagePaymentChartData(): any {
    return [
      {
        'name': this.translate.transform('FromOwnCapital'),
        'value': this.calculations.myMortgagePayment,
        'share': this.calculations.monthlyMortgagePayment ? this.calculations.myMortgagePayment / this.calculations.monthlyMortgagePayment : 0,
        'color': '#1b262c'
      },
      {
        'name': this.translate.transform('FromRental'),
        'value': this.calculations.incomeMortgagePayment,
        'share': this.calculations.monthlyMortgagePayment ? this.calculations.incomeMortgagePayment / this.calculations.monthlyMortgagePayment : 0,
        'color': '#ff880e'
      }
    ]
  }
  
  share(): void {
    this.shareService.copyToClipboard(this.shareService.generateShareLink(this.ranges));
    this.snackBar.open(String(this.translate.transform('LinkWasCopiedToClipBoard')), 'OK', {
      duration: 3 * 1000,
    });
  }

  maxValueSelected(): void {
    if (this.maxValueCount < 3) {
      this.maxValueCount++;
      this.snackBar.open(String(this.translate.transform('HigherValueCanBePutInInput')), 'OK', {
        duration: 3 * 1000,
      });
    }
  }
}
