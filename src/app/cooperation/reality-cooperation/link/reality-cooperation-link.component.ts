import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { UnitType } from "src/app/utils/unit.service";
import { GeneratorRange } from "src/app/formsControls/generator-item/generatorRange";
import { faCopy } from "@fortawesome/free-solid-svg-icons";
import { RentalInvestmentRangeService } from "src/app/formsControls/range/rental-investment-range.service";
import { ShareService } from "src/app/services/share.service";
import { Generator } from "src/app/formsControls/generator-item/generator";

@Component({
    selector: 'reality-cooperation-link',
    templateUrl: './reality-cooperation-link.component.html',
    styleUrls: ['./reality-cooperation-link.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class RealityCooperationLinkComponent implements OnInit {

    unitType = UnitType;
    groupedGenerators: GeneratorRange[] = [];
    faCopy = faCopy;
    generatedLink: string;
  
    constructor(
      rangeService: RentalInvestmentRangeService,
      private shareService: ShareService,
    ) {
      rangeService.getGroupedRanges().subscribe(groupedRanges => {
        groupedRanges.forEach(group => {
          const groupGenerator = new GeneratorRange();
          groupGenerator.name = group.name;
          group.ranges.forEach(range => {
            groupGenerator.generators.push(new Generator(range));
          });
          this.groupedGenerators.push(groupGenerator);
        });
      });
    }
  
    ngOnInit() {
      this.generateLink();
    }
  
    public generatorChanged(): void {
      this.generateLink();
    }
  
    public copyLink(): void {
      this.shareService.copyToClipboard(this.generatedLink);
    }
  
    generateLink(): void {
      this.generatedLink = this.shareService.generateCooperationLink(this.groupedGenerators);
    }
}