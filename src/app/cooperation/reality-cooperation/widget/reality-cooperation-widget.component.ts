import { Component } from "@angular/core";
import { RentalInvestmentRangeService } from "src/app/formsControls/range/rental-investment-range.service";
import { GeneratorRange } from "src/app/formsControls/generator-item/generatorRange";
import { Generator } from "src/app/formsControls/generator-item/generator";
import { ShareService } from "src/app/services/share.service";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";

@Component({
    selector: 'reality-cooperation-widget',
    templateUrl: './reality-cooperation-widget.component.html',
    styles: [`
    `]
})
export class RealityCooperationWidgetComponent {
    
    groupedGenerators: GeneratorRange[] = [];
    linkToWidgetPreview: SafeUrl;
    widgetColor: string;
    widgetHeight: number;
    widgetWidth: number;
  
    constructor(
      rangeService: RentalInvestmentRangeService,
      private shareService: ShareService,
      private sanitizer: DomSanitizer,
    ) {
      rangeService.getGroupedRanges().subscribe(groupedRanges => {
        groupedRanges.forEach(group => {
          const groupGenerator = new GeneratorRange();
          groupGenerator.name = group.name;
          group.ranges.forEach(range => {
            groupGenerator.generators.push(new Generator(range));
          });
          this.groupedGenerators.push(groupGenerator);
        });
        
        this.generateIframeLink();
      });
    }

    generatorChanged(): void {
      this.generateIframeLink();
    }

    generateIframeLink(): void {
      this.linkToWidgetPreview = this.sanitizer.bypassSecurityTrustResourceUrl(this.shareService.generateWidgetLink(this.groupedGenerators, [`color=${this.widgetColor}`]));
      console.warn(this.linkToWidgetPreview);
    }

}