export class RentalPropertyCalculations {

    //input values
    public propertyPrice: number;
    public ownCapital: number;
    public comission: number;
    public interestRate: number;
    public longTerm: number;
    public mortgageInsurance: number;
    public expectedRental: number;
    public rentalFees: number;
    public lawPartner: number;
    public propertyInsurance: number;
    public ownershipTax: number;
    public purchaseTaxRate: number;
    public rentalTaxRate: number;

    //calculated values
    public purchaseTaxAbs: number;
    public comissionAbs: number;
    public totalPrice: number;
    public loanNeeded: number;
    public mortgageTotal: number;
    public interestAbs: number;
    public rentalTaxAbs: number;
    public lawPartnerAbs: number;

    public myMortgagePayment: number;
    public myMortgageTotal: number;
    public incomeMortgagePayment: number;
    public monthlyMortgagePayment: number;

    public paidEarlier: boolean;
    public monthlySavings: number;

    public monthlyCosts: number;
    public yearlyCosts: number;
    public totalCostsMonthly: number;
    public monthlyNettIncome: number;
    public totalNettIncome: number;

    public initialInvestment: number;
    public additionalInvestment: number;
    public investmentCostsOwner: number;
    public investmentCostsTenant: number;
    public investmentCostsTotal: number;

    public nettYearlyRoa: string;
}