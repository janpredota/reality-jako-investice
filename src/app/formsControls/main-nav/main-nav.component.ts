import { Component, OnInit, ViewChild, HostListener, ElementRef, EventEmitter, ViewEncapsulation } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, NavigationEnd } from '@angular/router';
import { MatSidenav, MatSelectChange } from '@angular/material';
import { TranslateService } from 'src/app/services/translate.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css'],
})
export class MainNavComponent {
  
  @ViewChild('drawer', {static: false}) drawer: MatSidenav;
  @ViewChild('main', {static: false}) main: ElementRef;

  langOptions: string[] = TranslateService.languageOptions;
  defaulLanguage: string = TranslateService.defaultLanguage;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private translateService: TranslateService,
    ) {
      this.router.events.subscribe(event => {
      // close sidenav on routing
      this.drawer.close();});
  }

  ngOnInit() {
      this.router.events.subscribe((evt) => {
          if (!(evt instanceof NavigationEnd)) {
              return;
          }
          this.main.nativeElement.scrollIntoView();
      });
      if (this.drawer) {
        this.drawer.close();
      }
  }

  setLang(event: MatSelectChange) {
    this.translateService.use(event.value);
  }
}
