import { Injectable } from '@angular/core';
import { Range } from './range';
import { RangeGroup } from './rangeGroup';
import { Observable, of } from 'rxjs';
import { UnitType } from 'src/app/utils/unit.service';

@Injectable({
  providedIn: 'root'
})
export class RentalInvestmentRangeService {
  propertyPrice: Range = new Range('PurchasePrice', 'propertyPrice', 10_0000, 5_000_000, 9_000_000, 100_000, UnitType.czechCurrency, true, 'Tržní cena a cena nemovitosti, za kterou se nemovitost prodává, se mohou lišit');
  ownCapital: Range = new Range('OwnCapital', 'ownCapital', 0, 1_000_000, 5_000_000, 100_000, UnitType.czechCurrency, true, 'Peníze, které máte k dispozici a chcete je investovat');
  interestRate: Range = new Range('InterestRate', 'interestRate', 0, 0.03, 0.06, 0.1, UnitType.percentage);
  longTerm: Range = new Range('LongTerm', 'longTerm', 1, 20, 30, 1, UnitType.years);
  expectedRental: Range = new Range('ExpectedRental', 'expectedRental', 500, 15_000, 40_000, 500, UnitType.czechCurrencyPerMonth, true, 'Očekávaný příjem z pronájmu nemovitosti');
  commision: Range = new Range('RealityAgencyCommission', 'commision', 0, 0.03, 0.08, 0.1, UnitType.percentage, false, '', true, false);
  rentalFees: Range = new Range('FeesPaidByOwner', 'rentalFees', 0, 2_500, 7_000, 100, UnitType.czechCurrencyPerMonth, true, 'Všechny poplatky, které bude platit majitel nemovitosti (energie, fond oprav, ..) a jsou součástí nájmu', true, true);
  lawPartner: Range = new Range('LawPartner', 'lawPartner', 0, 0.10, 0.3, 1, UnitType.percentage, false, 'Všechny poplatky, které bude platit majitel nemovitosti (energie, fond oprav, ..) a jsou součástí nájmu', true, false);
  taxOwnership: Range = new Range('OwnershipTax', 'taxOwnership', 0, 1_000, 5_000, 100, UnitType.czechCurrencyPerYear, true, 'Platí se každý rok a v závislosti na ploše, počtu pater a místě nemovitosti', true, true);
  insuranceFlat: Range = new Range('PropertyInsurance', 'insuranceFlat', 0, 1_200, 7_000, 100, UnitType.czechCurrencyPerYear, false, '', true, false);
  insuranceMortage: Range = new Range('MortgageInsurance', 'insuranceMortage', 0, 1_100, 7_000, 100, UnitType.czechCurrencyPerYear, false, '', true, false)
  taxPurchase: Range = new Range('PurchaseTax', 'taxPurchase', 0, 0.04, 0.2, 1, UnitType.percentage, true, 'Pro rok 2018 je sazba 4 %', true, true, true);
  taxRental: Range = new Range('RentalTax', 'taxRent', 0, 0.15, 0.25, 1, UnitType.percentage, true, 'Pro rok 2018 je sazba 15 %', true, false, true);

  ranges: Range[] = [
    this.propertyPrice,
    this.ownCapital,
    this.interestRate,
    this.longTerm,
    this.expectedRental,
    this.commision,
    this.rentalFees,
    this.lawPartner,
    this.taxOwnership,
    this.insuranceFlat,
    this.insuranceMortage,
    this.taxPurchase,
    this.taxRental,
  ]

  investmentCostsGroup: RangeGroup = {name: 'InvestmentCosts', ranges: [this.propertyPrice, this.ownCapital, this.commision]};
  mortageGroup: RangeGroup = {name: 'BankLoan', ranges: [this.interestRate, this.longTerm, this.insuranceMortage]};
  rentalConditionsGroup: RangeGroup = {name: 'RentalConditions', ranges: [this.expectedRental, this.rentalFees, this.lawPartner, this.insuranceFlat]};
  taxesGroup: RangeGroup = {name: 'Taxes', ranges: [this.taxOwnership, this.taxPurchase, this.taxRental]};

  groupedRanges: RangeGroup[] = [
    this.investmentCostsGroup,
    this.mortageGroup,
    this.rentalConditionsGroup,
    this.taxesGroup
  ]

  getGroupedRanges(): Observable<RangeGroup []> {
    return of(this.groupedRanges);
  }
}
