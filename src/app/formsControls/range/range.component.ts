import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Range } from 'src/app/formsControls/range/range';
import { MatCheckboxChange } from '@angular/material';
import { faInfoCircle, faTimes, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import { UnitType } from 'src/app/utils/unit.service';

const maskConfig = {
  prefix: '',
  suffix: '',
  includeThousandsSeparator: true,
  thousandsSeparatorSymbol: ' ',
  requireDecimal: false,
  allowNegative: false,
  allowLeadingZeroes: false,
}

@Component({
  selector: 'app-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.css'],
})
export class RangeComponent implements OnInit, AfterViewInit {

  currencyMask = createNumberMask({
    ...maskConfig
  });

  @Input() range: Range;
  @Output() change: EventEmitter<void> = new EventEmitter();
  @Output() maxValue: EventEmitter<void> = new EventEmitter();
  @Input() color: string;
  precision: number = 0;
  faInfoCircle = faInfoCircle;
  faTimes = faTimes;
  faPlus = faPlus;
  faMinus = faMinus;
  hideClearInput: boolean = true;
  internalValue: number;
  internalValueInput: number;

  @ViewChild('value', {static: false}) valueInput:ElementRef;

  ngOnInit() {
    this.internalValue = this.range.value;
    this.internalValueInput = this.range.value;
    this.precision = this.range.step < 1 ? 2 : 0;
    if (!this.color) {
      this.color = '#0f4c75';
    }
    document.documentElement.style.setProperty('--my-color', this.color);
    if (this.range.unitType === UnitType.percentage) {
      this.currencyMask = createNumberMask({
        ...maskConfig,
        allowDecimal: true,
      })
    }
  }

  ngAfterViewInit() {
    if (!this.range.readOnly) {
      let rangeInput: any = document.getElementById('slider' + this.range.parameterName);
      rangeInput.value = this.range.value;
      rangeInput && rangeInput.addEventListener("input", () => this.sliderValueChanged(rangeInput.value), false);
      rangeInput && rangeInput.addEventListener("change", () => this.sliderValueChanged(rangeInput.value), false);
    }
  }

  sliderValueChanged(value: any) {
    this.internalValue = +value;
    this.internalValueInput = +value;
    this.rangeChanged();
    if (this.range.value === this.range.max) {
      this.maxValue.emit();
    }
  }

  clearInput(input): void {
    input.value = '';
    setTimeout(() => {
      this.valueInput.nativeElement.focus();
    }, 0);
  }

  onInputBlur(value): void {
    if (!value) {
      this.internalValue = this.range.min;
      this.internalValueInput = this.internalValue;
      this.rangeChanged();
    }
    setTimeout(() => {
      this.hideClearInput = true;
    }, 100);
  }

  onInputFocus(): void {
    this.hideClearInput = false;
  }

  inputFieldChanged(value: string): void {
    const numberValue = parseInt(value.replace(/ /g, ''), 10);
    if (numberValue) {
      this.internalValue = numberValue;
      this.rangeChanged();
    }
  }

  rangeChanged(): void {
    this.range.value = this.internalValue;
    this.setCheckFlag();
    this.change.emit();
  }

  optionalChange($event: MatCheckboxChange) {
    this.range.checked = $event.checked;
    this.change.emit();
  }

  private setCheckFlag() {
    if (this.range.optional) {
      this.range.checked = true;
    }
  }

  plus(input): void {
    this.internalValue += this.range.step;
    this.internalValueInput = this.internalValue;
    this.rangeChanged();
    setTimeout(() => {
      this.valueInput.nativeElement.focus();
    }, 0);
  }

  minus(input): void {
    const value = this.internalValue - this.range.step;
    this.internalValue = value < 0 ? 0 : value;
    this.internalValueInput = this.internalValue;
    this.rangeChanged();
    setTimeout(() => {
      this.valueInput.nativeElement.focus();
    }, 0);
  }
}
