import { Range } from "./range";

export class RangeGroup {
    name: string;
    ranges: Range[];
}