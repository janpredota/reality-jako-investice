import { UnitService, UnitType } from "src/app/utils/unit.service";

export class Range {

    name: string;
    parameterName: string;
    min: number;
    value: number;
    max: number;
    step: number;
    unitType: UnitType;
    optional: boolean = false;
    checked: boolean = false;
    readOnly: boolean = false;
    hidden: boolean = false;
    toolTipShow: boolean = false;
    toolTipMessage: string = '';

    constructor(name?: string, parameterName?: string, min?: number, value?: number, max?: number, step?: number, unitType?: UnitType, toolTipSHow?: boolean, toolTipMessage?: string, optional?: boolean, checked?: boolean, readOnly?: boolean) {
        this.name = name || '';
        this.parameterName = parameterName || '';
        this.min = this.getNiceNumber(min) || 0;
        this.unitType = unitType || UnitType.czechCurrency;
        this.value = this.getNiceNumber(value) || 0;
        this.max = this.getNiceNumber(max) || 0;
        this.step = step || 0;
        this.toolTipShow = toolTipSHow || false;
        this.toolTipMessage = toolTipMessage || '';
        this.optional = optional || false;
        this.checked = checked || false;
        this.readOnly = readOnly || false;
    }

    public getRawValue(): number {
        return this.value;
    }

    public getValue(): number {
        return this.optional && !this.checked ? 0 : this.getTrueValue();
    }

    public getValueUnit(): string {
        return this.getNumberAndUnit(this.value);
    }

    public getValueUnitWithoutTime(): string {
        return this.getNumberAndUnitWithoutTime(this.value);
    }

    public getMinUnit(): string {
        return this.getNumberAndUnitWithoutTime(this.min);
    }

    public getMaxUnit(): string {
        return this.getNumberAndUnitWithoutTime(this.max);
    }

    private getNiceNumber(value: number): number {
        return this.unitType === UnitType.percentage ? value * 100 : value;
    }

    private getNumberAndUnit(value: number): string {
        return `${value.toLocaleString()} ${UnitService.getUnit(this.value, this.unitType)}`;
    }

    private getNumberAndUnitWithoutTime(value: number): string {
        return `${value.toLocaleString()} ${UnitService.getUnitWithoutTime(value, this.unitType)}`;
    }

    public getCurrentUnit(): string {
        return UnitService.getUnit(this.value, this.unitType);
    }

    private getTrueNumber(num: number): number {
        return this.unitType === UnitType.percentage ? num / 100 : num;
    }

    getTrueValue(): number {
        return this.getTrueNumber(this.value);
    }

    getTrueMin(): number {
        return this.getTrueNumber(this.min);
    }

    getTrueMax(): number {
        return this.getTrueNumber(this.max);
    }

    public static copyRangeValue(source: Range, target: Range): Range {
        target.name = source.name;
        target.parameterName = source.parameterName;
        target.min = source.min;
        target.value = source.value;
        target.max = source.max;
        target.step = source.step;
        target.unitType = source.unitType;
        target.toolTipShow = source.toolTipShow;
        target.toolTipMessage = source.toolTipMessage;
        target.optional = source.optional;
        target.checked = source.checked;
        target.readOnly = source.readOnly;
        return target;
    }

    public getRangeParameter(): string {
        if (this.optional && !this.checked) {
            return '';
        }

        return `${this.parameterName}=${this.value}`;
    }
}