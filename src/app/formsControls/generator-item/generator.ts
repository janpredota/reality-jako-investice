import { Range } from "../range/range";

export class Generator extends Range {
    enabled: boolean;
    ownValue: boolean;
    visible: boolean;

    constructor (range: Range, visible: boolean = false) {
        super(range.name, range.parameterName, range.min, range.getTrueValue(), range.max, range.step, range.unitType, range.toolTipShow, range.toolTipMessage, range.optional, range.checked, range.readOnly);
        this.enabled = false;
        this.ownValue = range.optional ? false : true;
        this.visible = visible;
    }

    public getGeneratorParameter(visibleToo: boolean = false): string {
        if (!this.enabled) {
            return '';
        }

        let parameter: string = this.ownValue ? `${this.parameterName}=${this.value}` : this.parameterName;

        if (visibleToo && this.visible) {
            parameter += `&${this.parameterName}Visible=${this.visible}`;
        }

        return parameter;
    }
}
