import { Generator } from "./generator";

export class GeneratorRange {
    name: string;
    generators: Generator[] = [];
}