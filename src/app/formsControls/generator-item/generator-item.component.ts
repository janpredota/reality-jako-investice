import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Generator } from './generator';
import { UnitType } from 'src/app/utils/unit.service';
import createNumberMask from 'text-mask-addons/dist/createNumberMask'

const maskConfig = {
  prefix: '',
  suffix: '',
  includeThousandsSeparator: true,
  thousandsSeparatorSymbol: ' ',
  requireDecimal: false,
  allowNegative: false,
  allowLeadingZeroes: false,
}

@Component({
  selector: 'app-generator-item',
  templateUrl: './generator-item.component.html',
  styleUrls: ['./generator-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GeneratorItemComponent implements OnInit {
  
  currencyMask = createNumberMask({
    ...maskConfig
  });

  precision: number = 0;
  defaultValue: number;

  @Input() generator: Generator;
  @Input() visibleField: boolean = false;
  @Output('change') changeEmitter: EventEmitter<void> = new EventEmitter();

  constructor() {
   }

  ngOnInit() {
    this.defaultValue = this.generator.value;
    if (this.generator.unitType === UnitType.percentage) {
      this.currencyMask = createNumberMask({
        ...maskConfig,
        allowDecimal: true,
      })
    }
    this.precision = this.generator.unitType === UnitType.percentage ? 2 : 0;
  }

  valueChanged(e): void {
    if (!this.generator.enabled) {
      this.generator.enabled = true;
    }
    if (this.generator.optional) {
      this.generator.ownValue = true;
    }
    this.changeEmitter.emit();
  }

  ownValueChanged(e): void {
    if (!this.generator.ownValue) {
      this.generator.value = this.defaultValue;
    }
    if (!this.generator.enabled) {
      this.generator.enabled = true;
    }
    this.changeEmitter.emit();
  }

  enabledChanged(e): void {
    this.changeEmitter.emit();
  }

  visibleChanged(e): void {
    this.changeEmitter.emit();
  }

  private isReadOnly(): boolean {
    return !this.generator.enabled;
  }

  notEditableOwnValue(): boolean {
    return !this.generator.enabled || (this.generator.optional && !this.generator.ownValue);
  }

  
}
