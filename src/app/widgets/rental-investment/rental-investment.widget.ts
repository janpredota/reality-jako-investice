import { Component, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Range } from "src/app/formsControls/range/range"
import { RentalInvestmentRangeService } from "src/app/formsControls/range/rental-investment-range.service";
import { RentalService } from "src/app/services/rental.service";
import { RentalPropertyCalculations } from "src/app/models/rental-property-calculations";
import { HttpUtils } from "src/app/utils/http.utils";

@Component({
    selector: 'rental-investment-widget',
    templateUrl: './rental-investment.widget.html',
    styleUrls: ['./rental-investment.widget.css'],
})
export class RentalInvestmentWidget {

    propertyPrice: Range = new Range();
    ownCapital: Range = new Range();
    interestRate: Range = new Range();
    longTerm: Range = new Range();
    expectedRental: Range = new Range();
    commision: Range = new Range();
    rentalFees: Range = new Range();
    lawPartner: Range = new Range();
    taxOwnership: Range = new Range();
    insuranceFlat: Range = new Range();
    insuranceMortage: Range = new Range();
    taxPurchase: Range = new Range();
    taxRental: Range = new Range();

    ranges: Range[] = [
        this.propertyPrice,
        this.ownCapital,
        this.interestRate,
        this.longTerm,
        this.expectedRental,
        this.commision,
        this.rentalFees,
        this.lawPartner,
        this.taxOwnership,
        this.insuranceFlat,
        this.insuranceMortage,
        this.taxPurchase,
        this.taxRental,
      ]

    calculations: RentalPropertyCalculations = new RentalPropertyCalculations();

    monthlyMortgagePayment: number;
    myMonthlyMortgagePayment: number;
    yearlyRentability: number;

    myColor: string = 'red';

    constructor(
        route: ActivatedRoute,
        rangeService: RentalInvestmentRangeService,
        private rentalService: RentalService,
    ) {

        this.propertyPrice = Range.copyRangeValue(rangeService.propertyPrice, this.propertyPrice);
        this.ownCapital = Range.copyRangeValue(rangeService.ownCapital, this.ownCapital);
        this.interestRate = Range.copyRangeValue(rangeService.interestRate, this.interestRate);
        this.longTerm = Range.copyRangeValue(rangeService.longTerm, this.longTerm);
        this.expectedRental = Range.copyRangeValue(rangeService.expectedRental, this.expectedRental);
        this.commision = Range.copyRangeValue(rangeService.commision, this.commision);
        this.rentalFees = Range.copyRangeValue(rangeService.rentalFees, this.rentalFees);
        this.lawPartner = Range.copyRangeValue(rangeService.lawPartner, this.lawPartner);
        this.insuranceMortage = Range.copyRangeValue(rangeService.insuranceMortage, this.insuranceMortage);
        this.insuranceFlat = Range.copyRangeValue(rangeService.insuranceFlat, this.insuranceFlat);
        this.taxOwnership = Range.copyRangeValue(rangeService.taxOwnership, this.taxOwnership);
        this.taxPurchase = Range.copyRangeValue(rangeService.taxPurchase, this.taxPurchase);
        this.taxRental = Range.copyRangeValue(rangeService.taxRental, this.taxRental);

        route.queryParams.subscribe(params => {
            HttpUtils.setParamOrDefaultValue(params, this.propertyPrice);
            HttpUtils.setParamOrDefaultValue(params, this.ownCapital);
            HttpUtils.setParamOrDefaultValue(params, this.interestRate);
            HttpUtils.setParamOrDefaultValue(params, this.longTerm);
            HttpUtils.setParamOrDefaultValue(params, this.expectedRental);
            HttpUtils.setParamOrDefaultValue(params, this.taxPurchase);
            HttpUtils.setParamOrDefaultValue(params, this.commision);
            HttpUtils.setParamOrDefaultValue(params, this.rentalFees);
            HttpUtils.setParamOrDefaultValue(params, this.lawPartner);
            HttpUtils.setParamOrDefaultValue(params, this.insuranceFlat);
            HttpUtils.setParamOrDefaultValue(params, this.insuranceMortage);
            HttpUtils.setParamOrDefaultValue(params, this.taxRental);
            HttpUtils.setParamOrDefaultValue(params, this.taxOwnership);

            this.myColor = params['color'] || this.myColor;
            console.warn(this.myColor);

            this.recalculate();
        });
    }

    public recalculate(): void {

        this.calculations.propertyPrice = this.propertyPrice.getValue();
        this.calculations.ownCapital = this.ownCapital.getValue();
        this.calculations.comission = this.commision.getValue();
        this.calculations.interestRate = this.interestRate.getValue();
        this.calculations.longTerm = this.longTerm.getValue();
        this.calculations.mortgageInsurance = this.insuranceMortage.getValue();
        this.calculations.expectedRental = this.expectedRental.getValue();
        this.calculations.rentalFees = this.rentalFees.getValue();
        this.calculations.lawPartner = this.lawPartner.getValue();
        this.calculations.propertyInsurance = this.insuranceFlat.getValue();
        this.calculations.ownershipTax = this.taxOwnership.getValue();
        this.calculations.purchaseTaxRate = this.taxPurchase.getValue();
        this.calculations.rentalTaxRate = this.taxRental.getValue();

        this.rentalService.calculate(this.calculations);
    }
}