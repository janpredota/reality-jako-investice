import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CurrencyMaskModule } from 'ng2-currency-mask';

import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RentalInvestmentComponent } from './calculators/rental-investment/rental-investment.component';
import { RangeComponent } from './formsControls/range/range.component';
import { CalculatorList } from './calculators/calculator-list';
import { CalculatorComponent } from './calculators/calculators-overview.component';
import { GeneratorItemComponent } from './formsControls/generator-item/generator-item.component';
import { RealityCooperationComponent } from './cooperation/reality-cooperation/reality-cooperation.component';
import { ArticlesComponent } from './articles/articles-overview/articles-overview.component';
import { IndexComponent } from './index/index.component';
import { CooperationOverviewComponent } from './cooperation/cooperation-overview/cooperation-overview.component';
import { ArticleDetailComponent } from './articles/article-detail/article-detail.component';
import { ArticlePreviewComponent } from './articles/article-preview/article-preview.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { MatButtonModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule, MatSliderModule, MatPaginatorModule, MatCheckboxModule, MatExpansionModule, MatSelectModule, MatStepperModule, MatTabsModule, MatPaginatorIntl, MatBadgeModule, MatInputModule, MatSnackBarModule } from '@angular/material';
import { MainNavComponent } from './formsControls/main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatPaginatorIntlCzech } from './articles/articles-overview/matPaginatorIntlCzech';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { TranslateService } from './services/translate.service';
import { TranslatePipe } from './services/translate.pipe';
import { ShareService } from './services/share.service';
import { RentalInvestmentWidget } from './widgets/rental-investment/rental-investment.widget';
import { CurrencyPipe, registerLocaleData } from '@angular/common';
import localeCs from '@angular/common/locales/cs';
import { MAT_TABS_CONFIG } from '@angular/material/tabs';
import { RealityCooperationWidgetComponent } from './cooperation/reality-cooperation/widget/reality-cooperation-widget.component';
import { RealityCooperationLinkComponent } from './cooperation/reality-cooperation/link/reality-cooperation-link.component';
import { TextMaskModule } from 'angular2-text-mask';

registerLocaleData(localeCs);

export function setupTranslateFactory(
  service: TranslateService): Function {
  return () => service.use(TranslateService.defaultLanguage);
}

@NgModule({
  declarations: [
    RentalInvestmentComponent,
    RangeComponent,
    CalculatorList,
    CalculatorComponent,
    GeneratorItemComponent,
    RealityCooperationComponent,
    ArticlesComponent,
    IndexComponent,
    CooperationOverviewComponent,
    ArticleDetailComponent,
    ArticlePreviewComponent,
    MainNavComponent,
    TranslatePipe,
    RentalInvestmentWidget,
    AppComponent,
    RealityCooperationWidgetComponent,
    RealityCooperationLinkComponent,
  ],
  imports: [
    NgxChartsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    CurrencyMaskModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatSelectModule,
    MatStepperModule,
    MatTabsModule,
    MatBadgeModule,
    FontAwesomeModule,
    MatInputModule,
    HttpClientModule,
    MatSnackBarModule,
    NgxMaskModule.forRoot(),
    TextMaskModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'cs'},
    { provide: 'window', useFactory: windowFactory },
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCzech },
    TranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [ TranslateService ],
      multi: true
    },
    TranslatePipe,
    ShareService,
    CurrencyPipe,
    { provide: MAT_TABS_CONFIG, useValue: { animationDuration: '0ms' } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function windowFactory() {
  return window;
}
