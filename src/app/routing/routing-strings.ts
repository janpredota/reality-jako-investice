export class RoutingStrings {
    public static hashStrategy = true;
    public static calculatorsRouting = 'kalkulacky';
    public static widgetsRouting = 'widgety';
    public static propertyRentingRouting = 'pronajem-nemovitosti';
}