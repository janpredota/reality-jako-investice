import { Injectable, Inject } from "@angular/core";
import { Generator } from "./../formsControls/generator-item/generator";
import { Range } from "./../formsControls/range/range";
import { GeneratorRange } from "../formsControls/generator-item/generatorRange";
import { RangeGroup } from "../formsControls/range/rangeGroup";
import { RoutingStrings } from "../routing/routing-strings";

@Injectable({
    providedIn: 'root'
})
export class ShareService {
    linkToCalculator: string;
    linkToWidget: string;

    constructor(@Inject('window') private window: Window) {
        this.linkToCalculator = `${RoutingStrings.hashStrategy ? '/#' : ''}/${RoutingStrings.calculatorsRouting}`;
        this.linkToWidget = `${RoutingStrings.hashStrategy ? '/#' : ''}/${RoutingStrings.widgetsRouting}/${RoutingStrings.propertyRentingRouting}`;
    }

    private addGeneratorParameter(params: string[], generator: Generator, visibleToo: boolean = false): void {
        if (generator.enabled) {
            params.push(generator.getGeneratorParameter(visibleToo));
        }
    }

    private addRangeParameter(params: string[], range: Range): void {
        if (!range.optional || (range.optional && range.checked)) {
            params.push(range.getRangeParameter());
        }
    }

    private createLinkFromParameters(params: string[], linkDestination: string): string {
        let link: string = `${(window.location.protocol ? window.location.protocol + "//" : "") + this.window.location.host}${linkDestination}`;
        if (params.length > 0) {
            link += `?${params.join('&')}`;
        }
        return link;

    }

    generateCooperationLink(groupedGenerators: GeneratorRange[]): string {
        let params: string[] = [];
        groupedGenerators.forEach(group => {
            group.generators.forEach(element => {
                this.addGeneratorParameter(params, element);
            });
        });
        return this.createLinkFromParameters(params, this.linkToCalculator);
    }

    generateShareLink(groupedRanges: RangeGroup[]): string {
        let params: string[] = [];
        groupedRanges.forEach(group => {
            group.ranges.forEach(range => {
                this.addRangeParameter(params, range);
            });
        })
        return this.createLinkFromParameters(params, this.linkToCalculator);
    }

    generateWidgetLink(groupedGenerators: GeneratorRange[], additionalParams?: string[]): string {
        let params: string[] = [];
        groupedGenerators.forEach(group => {
            group.generators.forEach(element => {
                this.addGeneratorParameter(params, element, true);
            });
        });
        return this.createLinkFromParameters(params.concat(additionalParams), this.linkToWidget);
    }

    copyToClipboard(toCopy: string): void {
        document.addEventListener('copy', (e: ClipboardEvent) => {
          e.clipboardData.setData('text/plain', (toCopy));
          e.preventDefault();
          document.removeEventListener('copy', null);
        });
        document.execCommand('copy');
    }
}