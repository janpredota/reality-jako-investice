import { Injectable } from '@angular/core';
import { TaxService } from '../utils/tax.service';
import { MathUtils } from '../utils/math.utils';
import { RentalPropertyCalculations } from '../models/rental-property-calculations';
import { MortgageService } from '../utils/mortage.service';

@Injectable({
    providedIn: 'root'
})
export class RentalService {

    constructor(
        private taxService: TaxService,
    ) {

    }

    public calculate(rental: RentalPropertyCalculations): void {
        rental.purchaseTaxAbs = Math.round(rental.propertyPrice * rental.purchaseTaxRate);
        rental.comissionAbs = Math.round(rental.propertyPrice * rental.comission);
        rental.totalPrice = rental.propertyPrice + rental.purchaseTaxAbs + rental.comissionAbs;
        rental.loanNeeded = MathUtils.valueOrZero(rental.totalPrice - rental.ownCapital);
        rental.monthlyMortgagePayment = MortgageService.getMonthlyPayment(rental.loanNeeded, rental.interestRate, rental.longTerm);
        rental.mortgageTotal = rental.monthlyMortgagePayment * rental.longTerm * 12;
        rental.interestAbs = MathUtils.valueOrZero(rental.mortgageTotal - rental.loanNeeded);
        rental.investmentCostsTotal = rental.totalPrice + rental.interestAbs;
        rental.initialInvestment = rental.investmentCostsTotal > rental.ownCapital ? rental.ownCapital : rental.investmentCostsTotal;
        rental.rentalTaxAbs = this.taxService.getMontlyIncomeTax(rental.expectedRental, rental.rentalTaxRate);
        rental.lawPartnerAbs = (rental.expectedRental - rental.rentalFees) * rental.lawPartner;
        rental.monthlyCosts = rental.rentalFees + rental.mortgageInsurance + rental.rentalTaxAbs + rental.lawPartnerAbs;
        rental.yearlyCosts = rental.ownershipTax + rental.propertyInsurance;
        rental.totalCostsMonthly = rental.monthlyCosts + Math.round(rental.yearlyCosts / 12);
        rental.monthlyNettIncome = rental.expectedRental - rental.totalCostsMonthly;
        rental.myMortgagePayment = MortgageService.getMyMonthlyPayment(rental.monthlyMortgagePayment, rental.monthlyNettIncome);
        rental.myMortgageTotal = !rental.myMortgagePayment ? 0 : rental.myMortgagePayment * rental.longTerm * 12;
        rental.monthlySavings = rental.myMortgagePayment <= 0 ? rental.monthlyNettIncome - rental.monthlyMortgagePayment : 0;
        rental.totalNettIncome = rental.monthlyNettIncome * rental.longTerm * 12;

        let additionalInvestment: number = rental.mortgageTotal - rental.totalNettIncome;
        if (additionalInvestment < 0) {
            rental.additionalInvestment = 0;
            rental.paidEarlier = true;
            rental.incomeMortgagePayment = rental.monthlyMortgagePayment;
            rental.investmentCostsTenant = rental.investmentCostsTotal - rental.initialInvestment;
        } else {
            rental.additionalInvestment = additionalInvestment;
            rental.incomeMortgagePayment = rental.monthlyNettIncome;
            rental.investmentCostsTenant = rental.totalNettIncome;
        }
        rental.investmentCostsOwner = rental.initialInvestment + rental.additionalInvestment;
        const roaPropertyValue = Math.round(((rental.investmentCostsTenant + rental.monthlySavings * rental.longTerm) / rental.investmentCostsOwner) / rental.longTerm * 100 * 10) / 10;
        rental.nettYearlyRoa = roaPropertyValue > 1_000 ? '> 1 000' : roaPropertyValue.toString();
        /*rental.showRoa = roaPropertyValue > 0 && Number.isFinite(roaPropertyValue);
        const roaRental = Math.round(rental.monthlyNettIncome * 12 / rental.investmentCostsOwner * 100 * 10) / 10;
        rental.rentabilityRoaRental = roaRental.toString();*/
    }
}