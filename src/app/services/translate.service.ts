import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class TranslateService {

  public static readonly languageOptions: string[] = ['cz', 'en', 'de'];
  public static defaultLanguage: string = 'cz';
  public static currentLanguage: string;

  data: any = {};

  constructor(private http: HttpClient) {

  }
  
  use(lang: string): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      const langPath = `assets/i18n/${lang || 'en'}.json`;

      this.http.get<{}>(langPath).subscribe(
        translation => {
          this.data = Object.assign({}, translation || {});
          resolve(this.data);
          TranslateService.currentLanguage = lang;
        },
        error => {
          this.data = {};
          resolve(this.data);
        }
      );
    });
  }
}