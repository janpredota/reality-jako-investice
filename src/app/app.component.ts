import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { RoutingStrings } from './routing/routing-strings';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styles: [
    `
      footer {
        border: none;
        border-top: solid 1px #e0e0e0;
        margin: 0px 10px;
        font-size: 0.7em
      }

      footer, footer a {
        color: #b3b3b3;
      }

      footer .container div {
        margin: 10px 0px;
      }

      @media all and (min-width: 650px) {
        footer{
          font-size: 0.9em;
        }
      }
    `
  ]
})
export class AppComponent {

  public static readonly email = 'honza@predota.eu';
  withouHeaderAndFooter: boolean = false;

  constructor(
    private router: Router
  ) {
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        if (event.url.startsWith(`/${RoutingStrings.widgetsRouting}`)) {
          this.withouHeaderAndFooter = true;
        } else {
          this.withouHeaderAndFooter = false;
        }
      }
    });
  }

  getEmail(): string {
    return AppComponent.email;
  }
}
