import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { ArticlesComponent } from './articles/articles-overview/articles-overview.component';
import { ArticleDetailComponent } from './articles/article-detail/article-detail.component';
import { CalculatorComponent } from './calculators/calculators-overview.component';
import { CooperationOverviewComponent } from './cooperation/cooperation-overview/cooperation-overview.component';
import { RealityCooperationComponent } from './cooperation/reality-cooperation/reality-cooperation.component';
import { RentalInvestmentWidget } from './widgets/rental-investment/rental-investment.widget';
import { RoutingStrings } from './routing/routing-strings';
import { RealityCooperationWidgetComponent } from './cooperation/reality-cooperation/widget/reality-cooperation-widget.component';
import { RealityCooperationLinkComponent } from './cooperation/reality-cooperation/link/reality-cooperation-link.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'blog', component: ArticlesComponent },
  { path: 'blog/:id', component: ArticlesComponent },
  { path: 'clanek/:id', component: ArticleDetailComponent },
  {
    path: RoutingStrings.calculatorsRouting, component: CalculatorComponent,
    /*children: [
      { path: '', redirectTo: 'pronajem-nemovitosti', pathMatch: 'full' },
      { path: 'pronajem-nemovitosti', component: RentalInvestmentComponent }
    ]*/
  },
  {
    path: 'spoluprace', component: CooperationOverviewComponent,
    children: [
      { path: '', redirectTo: 'realitni-makleri', pathMatch: 'full' },
      { path: 'realitni-makleri', component: RealityCooperationComponent,
        children: [
          { path: '', redirectTo: 'link', pathMatch: 'full' },
          //{ path: 'widget', component: RealityCooperationWidgetComponent }, - not ready for prod yet
          { path: 'link', component: RealityCooperationLinkComponent },
        ] },
      { path: 'financni-agenti', component: RealityCooperationComponent },
    ]
  },
  {
    path: 'widgety',
    children: [
      { path: RoutingStrings.propertyRentingRouting, component: RentalInvestmentWidget },
    ]
  },];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: RoutingStrings.hashStrategy})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }