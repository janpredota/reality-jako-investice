import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../article';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styles: [`
      .image {
        float: right;
        padding: 0px 0px 20px 20px;
      }

      button {
        margin-top: 20px;
      }
      
      .content {
        overflow: hidden;
        clear: both;
      }

      article {
        margin: 0px 10px;
      }
  `]
})
export class ArticleDetailComponent implements OnInit {

  private sub: any;
  article: Article;

  constructor(
    private articleService: ArticlesService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let articleId: number = +params['id'] || 1;
      this.article = this.articleService.getArticle(articleId);
    });
  }

}
