import { Injectable } from '@angular/core';
import { Article } from './article';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  private dummyArticles: Article[] = [
    {articleId: 100, picture: '../assets/images/about_img.jpg', date: 'January 27,2018', title: 'Newest article that was pubished recently', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: `Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent dapibus. Aliquam erat volutpat. In dapibus augue non sapien. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Integer malesuada. Curabitur vitae diam non enim vestibulum interdum. Integer vulputate sem a nibh rutrum consequat. Et harum quidem rerum facilis est et expedita distinctio. Nullam eget nisl. Maecenas lorem. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Pellentesque pretium lectus id turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.

    Fusce wisi. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Mauris dictum facilisis augue. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Maecenas lorem. Etiam bibendum elit eget erat. Vivamus ac leo pretium faucibus. Integer tempor. Cras elementum. Maecenas libero. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. In convallis. Duis risus.
    
    Nullam at arcu a est sollicitudin euismod. Duis viverra diam non justo. Maecenas lorem. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Curabitur vitae diam non enim vestibulum interdum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque arcu. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Quisque porta. Aenean vel massa quis mauris vehicula lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce consectetuer risus a nunc. Sed convallis magna eu sem. Integer lacinia. Praesent vitae arcu tempor neque lacinia pretium.
    
    Nam quis nulla. Pellentesque ipsum. Ut tempus purus at lorem. Integer lacinia. Nunc tincidunt ante vitae massa. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Duis pulvinar. Nam sed tellus id magna elementum tincidunt. Nulla est. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Nunc auctor. Integer tempor.
    
    In dapibus augue non sapien. In rutrum. Pellentesque ipsum. Etiam quis quam. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Integer pellentesque quam vel velit. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Fusce consectetuer risus a nunc. In rutrum. Maecenas lorem. Aliquam erat volutpat. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna.`},
    {articleId: 101, picture: '../assets/images/blog_img1.jpg', date: 'January 28,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 102, picture: '../assets/images/blog_img2.jpg', date: 'January 29,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 103, picture: '../assets/images/blog_img3.jpg', date: 'January 30,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 104, picture: '../assets/images/blog_img4.jpg', date: 'January 31,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 105, picture: '../assets/images/blog_img1.jpg', date: 'February 1,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 106, picture: '../assets/images/blog_img2.jpg', date: 'February 2,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 107, picture: '../assets/images/blog_img3.jpg', date: 'February 3,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 108, picture: '../assets/images/blog_img4.jpg', date: 'February 4,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 109, picture: '../assets/images/blog_img1.jpg', date: 'February 5,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'},
    {articleId: 110, picture: '../assets/images/blog_img2.jpg', date: 'February 6,2018', title: 'But I must explain to you how all this mistaken idea', contentPreview: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', contentFull: 'Full'}
  ];

  constructor() { }

  public getAllArticles(): Article[] {

    return this.dummyArticles;
  }

  public getArticles(count: number, page: number): Article[] {
    return this.dummyArticles.slice(count * (page - 1), count * (page - 1) + count);
  }

  public getArticlesCount(): number {
    return this.dummyArticles.length;
  }

  public getArticle(id: number): Article {
    let articles = this.getAllArticles().filter(article => article.articleId === id);
    if (articles || articles.length > 0) {
      return articles[0];
    }

    return null;
  }
}
