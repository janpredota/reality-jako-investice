export class Article {

    articleId: number;
    picture: string;
    date: string;
    title: string;
    contentPreview: string;
    contentFull: string;

}
