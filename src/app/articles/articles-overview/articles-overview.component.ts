import { Component, OnInit, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Article } from '../article';
import { ArticlesService } from '../articles.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-articles',
  templateUrl: './articles-overview.component.html',
  styleUrls: ['./../article-preview/article-preview.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ArticlesComponent implements OnInit, OnDestroy {

  private sub: any;

  @Input() pageSize: number = 5;
  @Input() onlyFirstFew: number = 0;
  currentPage: number = 0;
  articlesTotal: number = 0;

  newestArticle: Article;
  olderArticles: Article[];

  constructor(
    private articlesService: ArticlesService,
    private route: ActivatedRoute,
    private router: Router
  ) {

  }

  ngOnInit() {

    this.articlesTotal = this.articlesService.getArticlesCount();

    this.sub = this.route.params.subscribe(params => {
      this.currentPage = +params['id'] || 1;

      let articles: Article[] = [];
      if (this.onlyFirstFew === 0) {
        articles = this.articlesService.getArticles(this.pageSize, this.currentPage);
      } else {
        articles = this.articlesService.getArticles(this.onlyFirstFew, 1);
      }

      if (!this.onlyFirstFew && this.currentPage === 1 && articles.length > 0) {
        this.newestArticle = articles[0];
        this.olderArticles = articles.slice(1);
      } else {
        this.newestArticle = null;
        this.olderArticles = articles;
      }
    });
  }

  ngOnDestroy() {
    this.sub && this.sub.unsubscribe();
  }

  pageChange(event: PageEvent) {
    this.router.navigate(['blog', event.pageIndex + 1]);
  }

}
