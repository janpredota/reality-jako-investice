import { MatPaginatorIntl } from '@angular/material';
export class MatPaginatorIntlCzech extends MatPaginatorIntl {
    firstPageLabel = 'První stránka';
    lastPageLabel = 'Poslední stránka';
    itemsPerPageLabel = 'Článků na stránku';
    nextPageLabel = 'Další stránka';
    previousPageLabel = 'Předchozí stránka';

    getRangeLabel = (page: number, pageSize: number, length: number) => {
        if (length == 0 || pageSize == 0) {
            return `0 z ${length}`;
        }
        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
        return `${startIndex + 1} - ${endIndex} z ${length}`;
    }
}